package hsoft.events;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class MarketDataEventTest {

	private MarketDataEvent marketDataEvent;

	@BeforeEach
	public void setup() {
		Long q = Long.valueOf(1);
		Double p = 1.0;
		this.marketDataEvent = new MarketDataEvent("test",  q, p);
	}

	@Test
	public void shouldGetQuantity() {
		Long actualValue = marketDataEvent.getQuantity();

		assertEquals(Long.valueOf(1), actualValue);
	}

	@Test
	public void shouldGetPrice() {
		Double actualValue = marketDataEvent.getPrice();

		assertEquals(Double.valueOf(1.0), actualValue);
	}

	@Test
	public void shouldToString() {
		String actualValue = marketDataEvent.toString();

		assertNotNull(actualValue);
	}
}
