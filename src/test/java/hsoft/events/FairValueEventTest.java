package hsoft.events;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class FairValueEventTest {


	private FairValueEvent fairValueEvent;

	@BeforeEach
	public void setup() {
		this.fairValueEvent = new FairValueEvent("test", 1.0);
	}

	@Test
	public void shouldGetFareValue() {
		Double actualValue = fairValueEvent.getFareValue();
		Double expected = 1.0;
		assertEquals(expected, actualValue);
	}

	@Test
	public void shouldGetProductId(){
		String pid = fairValueEvent.getProductId();
		assertEquals("test", pid);
	}

	@Test
	public void shouldToString() {
		String actualValue = fairValueEvent.toString();

		assertNotNull(actualValue);
	}
}
