package hsoft;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;


public class VwapEventProcessorTest {

	private VwapEventProcessor vwapEventProcessor;

	@BeforeEach
	public void setup() {
		this.vwapEventProcessor = new VwapEventProcessor();
	}

	@Test
	public void shouldHandleMarketDataEvent() {
		String productId = "test";
		long quantity = 1;
		double price = 1.0;
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{ 
			assertFalse(true, "should not call back");
		};

		vwapEventProcessor.handleMarketDataEvent(productId, quantity, price, greaterVwap);

		// run normally
	}

	@Test
	public void abnormalMarketData_negative_quantity(){
		String productId = "test";
		long quantity = -1;
		double price = 1.0;
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{
			assertFalse(true, "should not call back");
		};

		double fairValue = 1.0;
		vwapEventProcessor.handleFairValueEvent(productId, fairValue, greaterVwap);

		vwapEventProcessor.handleMarketDataEvent(productId, quantity, price, greaterVwap);
	}

	@Test 
	public void abnormalMarketData_zero_quantity(){
		String productId = "test";
		long quantity = 0;
		double price = 1.0;
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{
			assertFalse(true, "should not call back");
		};

		double fairValue = 1.0;
		vwapEventProcessor.handleFairValueEvent(productId, fairValue, greaterVwap);

		vwapEventProcessor.handleMarketDataEvent(productId, quantity, price, greaterVwap);
	}

	@Test
	public void normalVwapGreaterThanFairValue() {
		String productId = "test";
		long quantity = 1;
		double price = 2.0;
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{
			assertSame("test", pid);
			assertEquals(Double.valueOf(2.0), vwap);
			assertEquals(Double.valueOf(1.0), fv);
		};

		double fairValue = 1.0;
		vwapEventProcessor.handleFairValueEvent(productId, fairValue, greaterVwap);

		vwapEventProcessor.handleMarketDataEvent(productId, quantity, price, greaterVwap);
	}

	@Test
	public void normalVwapPickAllWhenLessThan5MarketEvents(){
		String productId = "test";
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{
			assertSame("test", pid);
			assertEquals(Double.valueOf(3.0), vwap); // (1.0*1 + 2.0*1 + 3.0*1 + 4.0*1 + 5.0*1)/ (1 + 1 + 1 + 1 + 1) = 3.0
			assertEquals(Double.valueOf(2.9), fv);
		};

		double fairValue = 2.9;

		vwapEventProcessor.handleMarketDataEvent(productId, 1, 1.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 2.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 3.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 4.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 5.0, greaterVwap);

		vwapEventProcessor.handleFairValueEvent(productId, fairValue, greaterVwap);
	}

	@Test
	public void normalVwapPickOnlyLatest5MarketEvents() {
		String productId = "test";
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{
			assertSame("test", pid);
			assertEquals(Double.valueOf(1.0), vwap); // (1.0*1 + 1.0*1 + 1.0*1 + 1.0*1 + 1.0*1)/ (1 + 1 + 1 + 1 + 1) = 1.0
			assertEquals(Double.valueOf(0.9), fv);
		};

		double fairValue = 0.9;
		

		vwapEventProcessor.handleMarketDataEvent(productId, 1, 100.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 1.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 1.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 1.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 1.0, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent(productId, 1, 1.0, greaterVwap);

		vwapEventProcessor.handleFairValueEvent(productId, fairValue, greaterVwap);
	}


	@Test
	public void shouldHandleFairValueEvent() {
		String productId = "test";
		double fairValue = 1.0;
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{};

		vwapEventProcessor.handleFairValueEvent(productId, fairValue, greaterVwap);

		// run normally
	}

	@Test
	public void shouldNotCallbackIfNoPreviousFairValue() {
		String productId = "test";
		double fairValue = 1.0;
		GreaterVwapCallback<String,Double,Double> greaterVwap = (pid,vwap,fv)->{
			assertFalse(true);
		};

		vwapEventProcessor.handleFairValueEvent(productId, fairValue, greaterVwap);
		vwapEventProcessor.handleMarketDataEvent("test2", 1, 2.0, greaterVwap);

	}

}
