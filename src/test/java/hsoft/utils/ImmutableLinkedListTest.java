package hsoft.utils;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Iterator;

public class ImmutableLinkedListTest {

	private ImmutableLinkedList<Integer> immutableLinkedList;

	@BeforeEach
	public void setup() {
		this.immutableLinkedList = new ImmutableLinkedList<Integer>();
	}

	@Test
	public void shouldInsertHead() {
		Integer value = 100;

		immutableLinkedList.insertHead(value);

		assertEquals(Integer.valueOf(100) , immutableLinkedList.getHead().get());
	}

	@Test
	public void shouldGetHead() {
		Node<Integer> actualValue = immutableLinkedList.getHead();

		assertEquals(null, actualValue);
	}

	@Test
	public void shouldIterator() {
		Iterator<Integer> actualValue = immutableLinkedList.iterator();

		assertNotEquals(null, actualValue);
	}
}
