package hsoft.utils;
import java.util.Iterator;

class NodeIterator<T> implements Iterator<T> {
    Node<T> current;

    // constructor
    NodeIterator(ImmutableLinkedList<T> obj) {
        this.current = obj.getHead();
    }
      
    // Checks if the next element exists
    public boolean hasNext() {
        return this.current!=null;
    }
      
    // moves the cursor/iterator to next element
    public T next() {
        T v = this.current.get();
        this.current = this.current.next();
        return v;
    }
      
    // Used to remove an element. Implement only if needed
    public void remove() throws UnsupportedOperationException {
        // Default throws UnsupportedOperationException.
        throw new UnsupportedOperationException("Do not support modification");
    }
}

public class ImmutableLinkedList<T> implements Iterable<T> {
    private Node<T> head;
    public ImmutableLinkedList(){
        this.head = null;
    }

    public void insertHead(T value){
        synchronized(this) {
            this.head = new Node<T>(value,this.head);
        }
    }

    public Node<T> getHead(){
        return head;
    }

    public Iterator<T> iterator() {
        return new NodeIterator<T>(this);
    }
}
