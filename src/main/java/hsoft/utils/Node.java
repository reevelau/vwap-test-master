package hsoft.utils;

public class Node<T> {
    private T value;
    private Node<T> next;
    public Node(T v, Node<T> n){
        this.value = v;
        this.next = n;
    }
    
    public T get() { return value; }
    public Node<T> next() { return next; }
}