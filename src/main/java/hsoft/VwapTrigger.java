package hsoft;

import com.hsoft.codingtest.DataProvider;
import com.hsoft.codingtest.DataProviderFactory;
import com.hsoft.codingtest.MarketDataListener;
import com.hsoft.codingtest.PricingDataListener;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class VwapTrigger {
  private static final VwapEventProcessor processor = new VwapEventProcessor();
  private static final Logger logger = LogManager.getLogger(VwapTrigger.class);

  public static void main(String[] args)  throws IOException{
    DataProvider provider = DataProviderFactory.getDataProvider();

    BufferedWriter writer = new BufferedWriter(new FileWriter("output_TEST_PRODUCT.txt"));
    final GreaterVwapCallback<String, Double, Double> callback = (pid, vwap, fv)->{
      if (pid=="TEST_PRODUCT"){
        String output = String.format("VWAP(%.14f) > FairValue(%.1f)", vwap, fv);
        logger.info(output);
        try {
          writer.write(output + "\n");
        }catch(IOException e) {
          logger.trace("Cannot write to output.txt");
        }
      }
    };

    provider.addMarketDataListener(new MarketDataListener() {
      public void transactionOccured(String productId, long quantity, double price) {
        processor.handleMarketDataEvent(productId, quantity, price, callback);
      }
    });
    provider.addPricingDataListener(new PricingDataListener() {
      public void fairValueChanged(String productId, double fairValue) {
        processor.handleFairValueEvent(productId, fairValue, callback);
      }
    });

    provider.listen();
    writer.close();
    // When this method returns, the test is finished and you can check your results in the console
    LogManager.shutdown();
  }
}