package hsoft;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;
import hsoft.events.VwapEvent;
import hsoft.events.FairValueEvent;
import hsoft.events.MarketDataEvent;
import hsoft.utils.ImmutableLinkedList;

class VwapEventProcessor {

  /**
   * Storage of interested events
   *   - single place of truth
   *   - simpified the calculation logic
   *   - segregate concurrecy control and computation processing.
   */
  private ImmutableLinkedList<VwapEvent> eventStore;

  public VwapEventProcessor() {
    this.eventStore =  new ImmutableLinkedList<VwapEvent>();
  }

  /**
   * Public function to reveive event from market price event listener
   * @param productId
   * @param quantity
   * @param price
   * @param greaterVwap, callback function when Vwap is greater than fair price of the product
   */
  public void handleMarketDataEvent(String productId, long quantity, double price,
      GreaterVwapCallback<String, Double, Double> greaterVwap) {
        synchronized(this) {
          this.eventStore.insertHead(new MarketDataEvent(productId, quantity, price));
          //events = this.extractSubList(this.eventStore, productId);
        }
        this.compareNotify( productId, greaterVwap);
  }

  /**
   * Public function to receive event from fair value event listener
   * @param productId
   * @param fairValue
   * @param greaterVwap, callback function when Vwap is greater than fair price of the product
   */
  public void handleFairValueEvent(String productId, double fairValue,
      GreaterVwapCallback<String, Double, Double> greaterVwap) {
        synchronized(this) {
          this.eventStore.insertHead(new FairValueEvent(productId, fairValue));
          //events = this.extractSubList(this.eventStore, productId);
        }
        this.compareNotify( productId, greaterVwap);
  }


  /**
   * Compare Vwap and fair value, execute callback when greater and callback is not null
   * @param productId
   * @param greaterVwap, callback function when Vwap is greater than fair price of the product
   */
  protected void compareNotify( String productId, GreaterVwapCallback<String, Double, Double> greaterVwap) {
    Double fv = this.getLastFareValue( productId);

    if (fv == null)
      return;

    Double vwap = this.calculateVwap( productId);

    if (vwap > fv && greaterVwap != null) {
      greaterVwap.apply(productId, vwap, fv);
    }
  }

  /**
   * Get latest fair value from the event storage of a specific productId
   * @param productId
   * @return
   */
  protected Double getLastFareValue( String productId) {

    return StreamSupport.stream(Spliterators.spliteratorUnknownSize(this.eventStore.iterator(), Spliterator.ORDERED), false)
        .filter(e -> ((e instanceof FairValueEvent) && e.getProductId() == productId))
        .limit(1)
        .map(FairValueEvent.class::cast)
        .map(fve -> fve.getFareValue())
        .findFirst()
        .orElseGet(() -> null);
  }

  /**
   * Calculate the Vwap by last 5 Market Price update events of a specific productId
   * @param productId
   * @return
   */
  protected Double calculateVwap( String productId) {

    Double VwapNumerator = StreamSupport.stream(Spliterators.spliteratorUnknownSize(this.eventStore.iterator(), Spliterator.ORDERED), false)
        .filter(e -> ((e instanceof MarketDataEvent) && e.getProductId() == productId))
        .limit(5)
        .map(MarketDataEvent.class::cast)
        .map(pmp -> pmp.getPrice() * pmp.getQuantity())
        .mapToDouble(Double::doubleValue)
        .sum();

    Long VwapDenominator = StreamSupport.stream(Spliterators.spliteratorUnknownSize(this.eventStore.iterator(), Spliterator.ORDERED), false)
        .filter(e -> ((e instanceof MarketDataEvent) && e.getProductId() == productId))
        .limit(5)
        .map(MarketDataEvent.class::cast)
        .map(pmp -> pmp.getQuantity())
        .mapToLong(Long::longValue)
        .sum();

    return VwapNumerator / VwapDenominator;
  }
}