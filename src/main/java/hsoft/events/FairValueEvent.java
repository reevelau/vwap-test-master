package hsoft.events;

public class FairValueEvent extends VwapEvent {
  private Double fareValue;

  public FairValueEvent(String pid, double fv) {
    this.productId = pid;
    this.fareValue = fv;
  }

  public Double getFareValue() {
    return this.fareValue;
  }

  @Override
  public String toString() {
    return String.format("FairValueEvent (productId: %s, fareValue: %.10f)", this.productId, this.fareValue);
  }
}