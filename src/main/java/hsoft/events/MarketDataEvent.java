package hsoft.events;

public class MarketDataEvent extends VwapEvent {
  private Long quantity;
  private Double price;

  public MarketDataEvent(String pid, Long q, Double p) {
    this.productId = pid;
    this.quantity = q;
    this.price = p;
  }

  public Long getQuantity() {
    return this.quantity;
  }

  public Double getPrice() {
    return this.price;
  }

  @Override
  public String toString() {
    return String.format("MarketDataEvent (productId: %s, quantity: %d, price: %.10f)", this.productId, this.quantity,
        this.price);
  }
}