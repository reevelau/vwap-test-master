package hsoft;

@FunctionalInterface
public interface GreaterVwapCallback<T, U, V> {
  public void apply(T t, U u, V v);
}